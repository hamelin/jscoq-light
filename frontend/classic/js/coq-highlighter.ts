// CM imports
import CodeMirror from "codemirror";
import './mode/coq-mode.js';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/runmode/runmode.js';

export class CoqHighlighter {
    highlight() {
        // Highlight, but do not create editor for code
        (async () => {
            const slides = document.getElementsByClassName("coq-highlight");
            for (let i = 0; i < slides.length; i++) {
                let element = slides.item(i) as HTMLElement;
                let code = element.textContent;
                element.textContent = "";
                CodeMirror.runMode(code, "coq", element);
            }
        })();
    }
}
