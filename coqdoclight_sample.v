(** **** Example coq source file using coqdoc *)

(** [[coqdoc]] is a tool to convert Coq source files into html or LaTeX documents.
We include in this repository an alternative implementation of coqdoc, [[coqdoclight.py]].
It does not generate LaTeX files, but it is able to generate interactive html documents using JsCoq.
This script requires no dependencies other than Python 3, and supports most coqdoc features:

- Titles (using leadings [[ * ]])
- Lists (using leadings [[ - ]])
  -- Including nested lists
- _Emphasis_
- #<marquee>Custom html tags</marquee>#
- Hiding code (see below for a more concrete example)
- Hiding code, allowing the user to reveal it (see (* begin details *) below)
- Inline coq code, for example: [forall Q : Prop, Q -> Q]
- Inline preformatted code, using (* [[ *) and (* ]] *)
- Verbatim using leadings [[<<]] and [[>>]]
- Horizontal rules to separates content

------------------------------------
↑ This is a horizontal rule
*)

(** The only features [[coqdoclight.py]] does not support is pretty-printing certain tokens;
It is normally registered using the special coqdoc comment:
<<
(** printing  *token* %...LATEX...% #...html...# *)
>>
But it will be ignored by [[coqdoclight.py]]
*)

(* begin read-only *)

Definition x := 3.

Goal forall (x:nat), x = x.
Proof.
intro x. reflexivity.
Qed.

(* end read-only *)

(* begin hide *)
(* This code is not shown in the document, but it is still ran *)
Definition universe_solution := 42.
(* end hide *)

(** [universe_solution] is defined in a hidden code block. *)

Check universe_solution.

(** We can also hide code, but still let the user reveal it if needed.
Notice that when the widget is in hidden state, all the code is ran at once, however if we open it,
we can go step by step
*)
(* begin details *)
Goal forall (x y z : nat), x = y -> y = z -> x = z.
Proof.
intros x y z h1 h2.
rewrite h1.
rewrite h2.
reflexivity.
Qed.
(* end details *)

(** 
We can also add a summary describing the snippet
*)

(* begin details : Proof that 42 is the solution to the universe *)
Goal 42 = universe_solution.
Proof.
reflexivity.
Qed.
(* end details *)
