(**
This file is part of the coq-teach library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** * Missing basic lemmas for the type [positive] of binary positive integers *)
From Coq Require Import PArith.BinPos.

Module Pos.
Local Open Scope positive_scope.

Lemma neq_1_lt_1 (p : positive) : p <> 1 <-> (1 < p).
Proof.
  split; [| now intros H H'; apply (Pos.lt_irrefl p); rewrite H' at 1].
  destruct p as [p | p |]; [| | intros C; exfalso; apply C]; reflexivity.
Qed.

Lemma le_le_succ_r (n m : positive) :
  n <= m -> n <= Pos.succ m.
Proof.
  intros H; apply Pos.le_trans with (1 := H).
  apply Pos.lt_le_incl; exact (Pos.lt_succ_diag_r _).
Qed.

Lemma le_succ_r (n m : positive) :
  (n <= Pos.succ m) <-> n <= m \/ n = Pos.succ m.
Proof.
  split.
  - now intros [I%Pos.lt_succ_r | ->]%Pos.le_lteq; [left | right; reflexivity].
  - intros [I | ->]; [apply le_le_succ_r; exact I | exact (Pos.le_refl _)].
Qed.

Lemma two_succ : 2 = Pos.succ 1.
Proof. reflexivity. Qed.

Lemma le_2_succ (n : positive) : 2 <= Pos.succ n.
Proof. rewrite two_succ, <-Pos.succ_le_mono; exact (Pos.le_1_l _). Qed.
End Pos.
