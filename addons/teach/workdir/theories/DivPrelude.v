(**
This file is part of the coq-teach library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

From Coq.ZArith Require Import BinInt Znumtheory.
From Coq Require Import Classical.
From Coq Require Import List.
Import ListNotations.
From Teach Require Import Zmod_div Zprime.
Export Znumtheory(prime).

(** * Preliminary lemmas and types for arithmetics exercises *)
#[local] Open Scope Z.

(** ** Missing lemmas about modular arithmetics *)

(** ** Numbers as non-empty strings of decimal digits *)

Inductive Digit :=
  d0 | d1 | d2 | d3 | d4 | d5 | d6 | d7 | d8 | d9.

Definition Z_of_Digit (c : Digit) :=
match c with
  d0 => 0
| d1 => 1
| d2 => 2
| d3 => 3
| d4 => 4
| d5 => 5
| d6 => 6
| d7 => 7
| d8 => 8
| d9 => 9
end.

Inductive Digits :=
| dsingleton (x : Digit) (* Un seul chiffre | A single digit *)
| dcons (x : Digit) (xs : Digits). (* Un chiffre suivi d'autres chiffres | A digit followed by other digits *)
(* Coercion dsingleton : Digit >-> Digits. *)

(*
⟪ = Ctrl+Shift+u 27EA
⟫ = Ctrl+Shift+u 27EB
*)
Notation "⟪ x ⟫" := (dsingleton x).
Notation "⟪ x ; .. ; w ; z ⟫" := (dcons x .. (dcons w (dsingleton z)) ..).

(* When compared to List.length, digits_len returns a Z rather than a nat *)
(* L'avantage par rapport à List.length : on renvoie un Z plutôt qu'un nat *)
Fixpoint digits_len (l : Digits): Z :=
  match l with
  | dsingleton _ => 1
  | dcons _ xs => (digits_len xs) + 1
  end.

(* ♯ = Ctrl+Shift+u 266F *)
Notation "♯ x" := (digits_len x) (at level 40).

Fixpoint digits_sum (cs : Digits) : Z :=
  match cs with
  | dsingleton d => Z_of_Digit d
  | dcons x xs => (Z_of_Digit x) + (digits_sum xs)
  end.

Fixpoint Z_of_Digits (l : Digits): Z :=
  match l with
  | dsingleton d => (Z_of_Digit d)
  | dcons x xs => (Z_of_Digit x)*10^(♯xs) + (Z_of_Digits xs)
  end.

Fixpoint shiftr (d : Digits) :=
  match d with
  | dsingleton d' => dsingleton d0
  | dcons h (dsingleton d') => dsingleton h
  | dcons h t => dcons h (shiftr t)
  end.

Fixpoint digits_canon (l : Digits) : Digits :=
  match l with
  | dcons d0 xs => digits_canon xs
  | l => l
  end.

Definition first_digit (xs : Digits) : Digit :=
  match xs with
  | dsingleton d => d
  | dcons x _ => x
  end.

Fixpoint last_digit (xs : Digits) : Digit :=
  match xs with
  | dsingleton d => d
  | dcons _ xs => last_digit xs
  end.

Definition findall {E} (P : E -> Prop) :=
  { l : list E | (Forall P l /\ (forall (x : E), P x -> In x l)) }.

Ltac atomize H := destruct H as [H | H]; [| try atomize H].

Definition not_In {E : Type} (x : E) (l : list E) := ~ In x l.


(** ** Missing lemmas about modular arithmetics *)
Module Z.

(* TODO: Is it really needed ? *)
Lemma square_opp (x : Z) : (- x) * (- x) = x * x.
Proof. rewrite Z.mul_opp_opp. reflexivity. Qed.

Lemma le_square_r (k : Z) : k <= k * k.
Proof.
  destruct (Z.nonpos_pos_cases k) as [H | H].
  - apply Z.le_trans with (1 := H); exact (Z.square_nonneg k).
  - rewrite <-(Z.mul_1_l k) at 1; apply Z.mul_le_mono_pos_r; [exact H |].
    apply Z.le_succ_l in H; rewrite Z.one_succ; exact H.
Qed.

Lemma mod_decomp (a b r : Z) (h : a mod b = r) :
  exists k, a = b * k + r.
Proof.
  destruct (Z.eq_dec b 0) as [-> | I].
  - exists a; rewrite Z.mul_0_l, Z.add_0_l.
    rewrite Z.mod_0_r in h; rewrite h; reflexivity.
  - exists (a / b); rewrite <-h; exact (Z.div_mod a b).
Qed.

Lemma zero_or_ge_1 (n : Z) : n <= - 1 \/ n = 0 \/ 1 <= n.
Proof.
  destruct n as [| p | p].
  - right; left; reflexivity.
  - right; right; apply Pos2Z.pos_le_pos; exact (Pos.le_1_l p).
  - left; apply Pos2Z.neg_le_neg; exact (Pos.le_1_l p).
Qed.

Lemma div_eucl_unique (a b q q' r r' : Z) :
  b <> 0 -> a = b * q + r -> a = b * q' + r' -> 0 <= r < b -> 0 <= r' < b
    -> q = q' /\ r = r'.
Proof.
  intros bn0 eq eq' ineq ineq'.
  rewrite eq in eq'.
  apply Z.add_move_r in eq'; rewrite <-Z.add_sub_assoc in eq'.
  apply eq_sym, Z.add_move_l in eq'.
  rewrite <-Z.mul_sub_distr_l in eq'.
  assert (- b < b * (q - q') < b) as key. {
    rewrite <-eq'; split.
    - apply Z.lt_add_lt_sub_l, (Z.lt_le_trans _ 0); [| now apply ineq'].
      apply Z.lt_add_lt_sub_r; rewrite Z.sub_opp_r, Z.add_0_l; now apply ineq.
    - apply Z.lt_sub_lt_add_l, (Z.lt_le_trans _ b); [now apply ineq' |].
      rewrite <-(Z.add_0_l b) at 1; apply Z.add_le_mono_r; now apply ineq.
  }
  assert (q - q' = 0) as qq'. {
    apply Z.abs_lt in key.
    assert (0 < b) as Ib. {
      apply Z.le_lt_trans with (2 := key).
      exact (Z.abs_nonneg _).
    }
    rewrite Z.abs_mul, Z.abs_eq in key by (now apply Z.lt_le_incl).
    apply Z.abs_0_iff.
    destruct (zero_or_ge_1 (Z.abs (q - q'))) as [I |[-> | I]].
    - exfalso; apply Z.le_ngt with (1:= Z.abs_nonneg (q - q')).
      apply Z.le_lt_trans with (1 := I).
      exact (Pos2Z.neg_is_neg _).
    - reflexivity.
    - exfalso; apply Z.lt_nge with (1 := key).
      apply Z.le_mul_diag_r; [exact Ib | exact I].
  }
  rewrite qq', Z.mul_0_r in eq'; split.
  - now apply Z.sub_move_0_r.
  - now apply eq_sym, Z.sub_move_0_r.
Qed.
(*
Lemma div_ (a b q r : Z) :
  b <> 0 -> a = b * q + r -> 0 <= r < b -> q = a / b /\ r = a mod b.
Proof.
  intros h1 h2 h3.
    Search (?a = ?b * ?q + ?r) (?r = ?a mod ?b).
    Search (?a = ?b * ?q + ?r) (?q = ?a / ?b).
  pose proof (Z.div_unique_pos a b q r h3 h2) as h4.

  pose proof (
  split.
  * 
*)

Lemma digits_sum_suiv (c : Digit) (cs : Digits) : digits_sum (dcons c cs) = (Z_of_Digit c) + digits_sum cs.
Proof. reflexivity. Qed.

Lemma Z_of_Digits_dsingleton (d : Digit) :
  Z_of_Digits (dsingleton d) = Z_of_Digit d.
Proof. reflexivity. Qed.

Lemma Z_of_Digits_dcons (h : Digit) (t : Digits) :
  Z_of_Digits (dcons h t) = (Z_of_Digit h) * 10 ^(♯t) + (Z_of_Digits t).
Proof. reflexivity. Qed.

Lemma shiftr_dsingleton (d : Digit) :
  shiftr (dsingleton d) = dsingleton d0.
Proof. reflexivity. Qed.

Lemma shiftr_two_digits (d d' : Digit) :
  shiftr (dcons d (dsingleton d')) = dsingleton d.
Proof. reflexivity. Qed.

Lemma shiftr_digits (h h' : Digit) (t : Digits) :
  shiftr (dcons h (dcons h' t)) = dcons h (shiftr (dcons h' t)).
Proof. reflexivity. Qed.

Lemma digits_len_digit (d : Digit) : digits_len (dsingleton d) = 1.
Proof. reflexivity. Qed.

Lemma digits_len_digits (h : Digit) (t : Digits) :
  digits_len (dcons h t) = (digits_len t) + 1.
Proof. reflexivity. Qed.

Lemma digits_sum_digit (d : Digit) :
  digits_sum (dsingleton d) = (Z_of_Digit d).
Proof. reflexivity. Qed.

Lemma digits_sum_digits (h : Digit) (t : Digits) :
  digits_sum (dcons h t) = (Z_of_Digit h) + digits_sum t.
Proof. reflexivity. Qed.

Lemma digits_len_shiftr_digits (h : Digit) (t : Digits) :
  digits_len (shiftr (dcons h t)) = digits_len t.
Proof.
  revert h; induction t as [d | d t' IH].
  - intros h; rewrite shiftr_two_digits, 2!digits_len_digit; reflexivity.
  - intros h; rewrite shiftr_digits, 2digits_len_digits, IH; reflexivity.
Qed.

Lemma digits_len_nneg (d : Digits) : 0 <= digits_len d.
Proof.
  induction d as [d | h t IH].
  - rewrite digits_len_digit; exact (Z.le_0_1).
  - rewrite digits_len_digits; apply Z.add_nonneg_nonneg;
      [exact IH | exact Z.le_0_1].
Qed.

Lemma digits_canon_correct (l : Digits) : Z_of_Digits l = Z_of_Digits (digits_canon l).
Proof.
  induction l as [x | h t IH]; [reflexivity |].
  destruct h; [simpl; exact IH | reflexivity..].
Qed.

Lemma last_digit_digit (d : Digit) :
  last_digit (dsingleton d) = d.
Proof. reflexivity. Qed.

Lemma last_digit_digits (h : Digit) (t : Digits) :
  last_digit (dcons h t) = last_digit t.
Proof. reflexivity. Qed.

Lemma decimal_decomposition (n : Digits) :
  Z_of_Digits n = 10 * (Z_of_Digits (shiftr n)) + (Z_of_Digit (last_digit n)).
Proof.
  induction n as [d | h t IH].
  - reflexivity.
  - destruct t as [d | h' t'].
    + rewrite shiftr_two_digits, last_digit_digits, last_digit_digit.
      rewrite Z_of_Digits_dcons, digits_len_digit, Z.pow_1_r.
      rewrite !Z_of_Digits_dsingleton, Z.mul_comm; reflexivity.
    + rewrite shiftr_digits, Z_of_Digits_dcons, digits_len_digits.
      rewrite (Z_of_Digits_dcons h), last_digit_digits, IH.
      rewrite digits_len_shiftr_digits, Z.add_assoc; f_equal.
      rewrite Z.mul_add_distr_l; f_equal.
      rewrite Z.pow_add_r; [| exact (digits_len_nneg _) | exact Z.le_0_1].
      rewrite Z.pow_1_r, Z.mul_assoc, Z.mul_comm; reflexivity.
Qed.

Lemma digits_canon_nonzero (l : Digits) :
  (digits_canon l) = ⟪ d0 ⟫ \/ first_digit (digits_canon l) <> d0.
Proof.
  induction l as [x | h t [IH0 | IHn0]].
  - destruct x; [left; reflexivity | right; intros H; discriminate H..].
  - destruct h; [left; simpl; exact IH0 | right; intros H; discriminate H..].
  - right; destruct h; simpl; [exact IHn0 | intros H; discriminate H..].
Qed.

Lemma In_dec {E : Type} (x : E) (l : list E) :
  (In x l) \/ (not_In x l).
Proof.
  induction l as [|y l [IH | IH]]; [right; intros [] |..].
  - left; right; exact IH.
  - destruct (classic (y = x)) as [-> | H].
    + left; left; reflexivity.
    + right; intros [H' | H']; [exact (H H') | exact (IH H')].
Qed.

Definition primeb_correct := Z.primeb_correct.
Definition not_prime_0 := not_prime_0.
Definition pair_equal_spec := pair_equal_spec.
Definition is_nonneg := Pos2Z.is_nonneg.
Definition prime_divisors := prime_divisors.
Definition prime_ge_2 := prime_ge_2.
Definition add_1_r := Z.add_1_r.
Definition add_mod := Z.add_mod.
Definition add_opp_diag_r := Z.add_opp_diag_r.
Definition divide_0_l := Z.divide_0_l.
Definition divide_add_cancel_l := Z.divide_add_cancel_l.
Definition divide_add_cancel_r := Z.divide_add_cancel_r.
Definition divide_add_r := Z.divide_add_r.
Definition divide_factor_l := Z.divide_factor_l.
Definition divide_factor_r := Z.divide_factor_r.
Definition divide_mul_l := Z.divide_mul_l.
Definition divide_mul_r := Z.divide_mul_r.
Definition divide_opp_r := Z.divide_opp_r.
Definition divide_pos_le := Z.divide_pos_le.
Definition divide_refl := Z.divide_refl.
Definition gauss := Z.gauss.
Definition mod_0_l := Z.mod_0_l.
Definition mod_divide := Z.mod_divide.
Definition mul_1_r := Z.mul_1_r.
Definition mul_assoc := Z.mul_assoc.
Definition mul_comm := Z.mul_comm.
Definition mul_mod := Z.mul_mod.
Definition mul_succ_r := Z.mul_succ_r.
Definition pow_1_l := Z.pow_1_l.
Definition pow_m1_l_odd := Z.pow_m1_l_odd.
Definition pow_mod := Z.pow_mod.
Definition divide_mul_cancel_l := Z.divide_mul_cancel_l.
End Z.

(* Tactics *)
Import Z.
Fixpoint range_pos (a b : Z) (n: nat) (acc: list Z) {struct n} : list Z :=
  match n with
  | O => a :: acc
  | S n' => range_pos a (b-1) n' (cons b acc)
  end.

Fixpoint range_neg (a b : Z) (n: nat) (acc: list Z) {struct n} : list Z :=
  match n with
  | O => b :: acc
  | S n' => range_neg (a+1) b n' (cons a acc)
  end.

Definition range (a b : Z) : list Z :=
  if a <=? b
  then range_pos a b (Z.to_nat (b-a)) nil
  else range_neg b a (Z.to_nat (a-b)) nil
.


#[local]
Ltac false_solv := match goal with
| [H : False |- _ ] => destruct H
end.

#[local]
Ltac casparcas' H := destruct H as [H | H]; [| try casparcas' H; try false_solv ].

#[local]
Ltac casparcas x l H :=
specialize (In_dec x l) as H; unfold In in H; unfold not_In in H; destruct H as [H|H]; [casparcas' H|].

(*Ltac casparcas_range x a b H :=
specialize (In_dec x (range a b)) as H; remember x as __x' in H; simpl in H; subst __x'; unfold In in H; unfold not_In in H; destruct H as [H|H]; [casparcas' H|]. *)

#[local]
Ltac casparcas_range x a b H :=
  specialize (In_dec x (range a b)) as H; remember x as __x' in H; destruct H as [H|H];
  [ unfold In in H; simpl in H; subst __x'; casparcas' H | unfold not_In in H; simpl in H; subst __x' ].


(*
La tactique `study` est conçue pour rendre plus simple l'analyse des valeurs d'une expression
Si on a un but de la forme Γ ⊢ G, et une expression E : T, alors on peut utilise la tactique
`study E in [a_1,a_2,...,a_n] as H` qui génerera (n+1) buts: le premier est:

Γ,H:E <> a1 /\ E <> a2 /\ ... /\ E <> a_n ⊢ G
Autrement dit, on doit prouver le but quand on sait que l'expression E
n'est égale à aucun des élements de la liste d'expression qu'on a donné.

Le second but est Γ,H:E = a1 ⊢ G, le troisième Γ,H:E = a2 ⊢ G ... et ainsi de suite jusqu'à Γ,H:E = an ⊢ G

Cette tactique est particulièrement utile dans le contexte des exercices sur la divisibilité, où étudier
les restes possibles d'une division euclidienne est courant pour prouver certaines propriétés.

~~~~

The study tactic is designed to make it easier to analyse the different possibles values of an expression
If we have a goal of the form Γ ⊢ G, and an expression E : T, then we can use the tactic
`study E in [a_1,a_2,...,a_n] as H` to generate (n+1) goals: the first one is

Γ,H:E <> a1 /\ E <> a2 /\ ... /\ E <> a_n ⊢ G : We have to prove the goal when we know that E is not in the list of expression.

The second goal is Γ,H:E = a1 ⊢ G, third is Γ,H:E = a2 ⊢ G and so on until Γ,H:E = an ⊢ G

This tactic is useful in the context of divisibility, where studying the range of remainder is common to
prove some properties.
*)
Tactic Notation "study" constr(x) "in" constr(l) "as" simple_intropattern(H) := casparcas x l H.

(*
`study E between a and b as H` est un sucre syntaxique pour `study E in [a,a+1,a+2,...,b] as H` (si a <= b) ou `[a,a-1,a-2,...,b]` (si a > b)

Il est courant d'analyser des intervalles continus lorsqu'on travaille sur la divisibilité:
Ce raccourcis permet d'éviter de faire des erreurs en listant les valeurs possibles d'un intervalle.

~~~

`study E between a and b as H` is a syntax sugar for `study E in [a,a+1,a+2,...,b] as H` (if a <= b) or [a,a-1,a-2,...,b] (if a > b)

It is common to have contiguous ranges when working with divisibility:
This syntax makes it less error-prone to list all possible values in a range.
*)
Tactic Notation "study" constr(x) "between" constr(a) "and" constr(b) "as" simple_intropattern(H) := casparcas_range x a b H.

Add Search Blacklist "Coq".

Require Import Zify.
